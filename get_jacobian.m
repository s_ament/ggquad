function [ J, S ] = get_jacobian( x, w, n, basis_struct )
% get_jacobian computes the Jacobian of the nonlinear system used to
% determine the generalized Gaussian quadrature with n nodes x and weights w

rank = basis_struct.rank;
u = basis_struct.b_cheb(x);
u_der = basis_struct.b_cheb_prime(x);

J = zeros( rank, 2*n );

J(:,1:n) = transpose( bsxfun(@times, u_der, transpose(w)) );
J(:,n+1:2*n) = transpose(u);

S =  transpose( w*u - basis_struct.basis_integrals );

end

