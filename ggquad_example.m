% Specify input set of functions. Note that the input functions have a
% singularity of type x^(-1/2) at x = 0.
f_in = @(x) evaluate_input_functions(x);

% Define the weight function.
w_in = @(x) 1;

% You could also do this without loss of precision:
%w_in = @(x) log(1+x);

% Specify domain of input functions. If more than 2 values are provided,
% the intermediate points will be used as break points for the piecewise
% Chebyshev series of the input functions.
domain = [-1, 0, 1];

% Lastly, we have to specify the exponents of the input functions at the
% domain points. This allows us to deal with singularities of type x^(-a).
% In this case, the functions have a singularity of type -.5 at 0, so we
% write:
exps = [0, -.5, 0];

% Now we can call ggquad. The first two arrays that are returned from a
% generalized Gaussian rule, while the second two form a generalized
% Chebyshev rule for the input set of functions.
[gg_x, gg_w, ch_x, ch_w] = ggquad(f_in, w_in, domain, exps);

% Let's look at the length of the quadratures:
display(sprintf('\nLength of the generalized Chebyshev rule: %d',length(ch_x)))
display(sprintf('Length of the generalized Gaussian rule: %d',length(gg_x)))

% We can calculate the integrals of the input functions with our new rule:
gg_I = gg_w * evaluate_input_functions( gg_x );

format long;
display([sprintf('Integrals of the first 3 input functions:\n') ...
    sprintf('1: %.15e\n', gg_I(1)) ... 
    sprintf('2: %.15e\n', gg_I(2)) ...
    sprintf('3: %.15e', gg_I(3)) ]);
display(['which is accurate to 15 digits (check with Wolfram Alpha), ' ...
    'despite the singularity.'])