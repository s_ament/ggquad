function [ ch_x, ch_w ] = calc_cheb_quad( U, rank, basis_integrals, pw_x, pw_w )
% calc_cheb_quad calculates the chebyshev quadrature for the functions
% defined by their values U(:,n)

[Q, R, e] = qr( transpose ( U(:, 1:rank) ), 'vector' ) ;

opts.UT = true;

z = linsolve( R(:, 1:rank), Q' * transpose(basis_integrals) , opts );

ch_w = transpose(z) .* sqrt( pw_w( e(1:rank) ) ) ;
ch_x = pw_x( e(1:rank) );
   
% Sorting the Nodes in ascending order
[ch_x, index] = sort(ch_x);
ch_w = ch_w(index);

end

