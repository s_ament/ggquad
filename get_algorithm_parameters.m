function [ algo_parameters ] = get_algorithm_parameters( )
% Defines and returns parameters which are used for algorithm

int = [-1,1];  % interval of integration

comp_eps = 1e-14; % compression precision

opt_eps = 1e-13; % optimization precision
newton_max_it = 20; % maximum number of iterations of newton algorithm
back_max_it = 40; % maximum number of backtracking steps in newton algorithm
back_factor = 2;  % Factor with which step size is decreased in backtracking
step_init = 1;  % Initial step size for Newton step

can_split = 'on'; 

quad_name = 'quad_test';

% Name to be used for output file
algo_parameters = struct('int', int, 'can_split', can_split, ...
                    'comp_eps', comp_eps, 'opt_eps', opt_eps, ...
                    'newton_max_it', newton_max_it,...
                    'back_max_it', back_max_it,...
                    'back_factor', back_factor,...
                    'step_init', step_init,...
                    'quad_name', quad_name);

end

