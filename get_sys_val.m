function [ S ] = get_sys_val( x, w, basis_struct)
% get_sys_val computes and returns the value of the nonlinear system at x,w

u = basis_struct.b_cheb( x );

S = transpose( w*u - basis_struct.basis_integrals );

end

