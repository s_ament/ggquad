A classical Gaussian quadrature is a set of n nodes {x_n} and n weights {w_n}, 
which can integrate polynomials of degree up to 2n-1 exactly.
This code allows for the construction of generalized Gaussian quadratures
for a large class of functions, not just polynomials. 
In particular, given a set of input functions {f_n}, and a weight function w, 
ggquad constructs a (near) optimal number of nodes and weights which can
integrate the input functions close to machine precision.
The general approach of the code was introduced here 
https://www.math.ucdavis.edu/~bremer/papers/quadrature.pdf.

The code relies on some functions of the Chebfun system, which can be downloaded
here: http://www.chebfun.org/download/

ggquad allows for the construction of quadratures for singular integrands
to virtually machine precision,
if the type of the singularity is the same for all input functions.
For example, all functions can have a logarithmic singularity at the same point,
or a singularity of type x^(-a), where a is the same for all functions.

The script ggquad_example.m shows how to use the code to construct quadratures
for a set of functions specified in evaluate_input_functions.m.
In addition, it demonstrates that accurate quadratures for singular integrands
can be calculated.

Some runtime parameters, like the required precision of the quadrature,
can be adjusted in get_algorithm_parameters.m

