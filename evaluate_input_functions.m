function [ FX ] = evaluate_input_functions( x )
%evaluate_input_functions computes all user specified input functions. The
%output FX is a matrix which holds the values of the nth function at x in
%its nth column.

numf = 32;
FX = zeros(length(x), numf);

for i = 1:numf
    FX(:,i) = cos( pi * i * x );
end

FX = bsxfun(@rdivide, FX, sqrt(abs(x)));

end

