function [ gg_x, gg_w, ch_x, ch_w ] = ggquad( f_in, w_in, domain, exps )
% ggquad calculates a generalized gaussian quadrature rule

% get algorithm parameters
algo_par = get_algorithm_parameters( );

algo_par.int = [domain(1), domain(length(domain))];

% if a singularity is specified, the code will be slower, as Chebfun does
% not allow for vectorized construction of singular chebfuns
display('Interpolating input functions.')
if any( exps ~= 0 )
    % If there are singularities in the input functions, absorb them
    % into pw_weightfun
    f_cheb = chebfun( @(x) bsxfun(...
                    @rdivide, f_in(x), pw_weightfun(x, domain, exps)...
                    ), domain, 'splitting', algo_par.can_split);
else  
    f_cheb = chebfun( f_in, domain, 'splitting', algo_par.can_split);
end

% get order of piecewise chebyshev expansion and multiply them by two, as
% we want to integrate products of functions with the resulting quadrature
f_cheb_coeffs = get( extractColumns( f_cheb, 1), 'coeffs');
f_exp_order = zeros( length( f_cheb_coeffs ), 1);

if iscell( f_cheb_coeffs ) 
    for ind = 1 : length( f_cheb_coeffs )
        f_exp_order(ind) = 2*length( f_cheb_coeffs{ind} );
    end
else
    f_exp_order = 2*length(f_cheb_coeffs);
end

total_length = sum( f_exp_order );

f_cheb_domain = f_cheb.domain;
min_interval_length = min( f_cheb_domain(2:length(f_cheb_domain)) - ...
                            f_cheb_domain(1:length(f_cheb_domain)-1) );
if min_interval_length < 1e-5
    display([sprintf('Warning: the length of the smallest interval on which\n') ...
            sprintf('the input functions are interpolated is %.1e. \n',...
            min_interval_length) ...
            sprintf('This may lead to inaccurate quadratures.\n') ...
            sprintf('To achieve better accuracy, try to absorb badly\n') ...
            sprintf('behaved parts of the function into the weight function,\n')...
            'or choose a different contour of integration.']);
end

% get piecewise clenshaw-curtis quadrature
[pw_x, pw_w] = chebpts(f_exp_order, f_cheb_domain);

sqrt_pw_w = transpose( sqrt(pw_w) );

% Set up matrix of discretized functions for svd  

A = bsxfun(@times, sqrt_pw_w, f_cheb( pw_x ) );

num_functions = size(A,2);

naninf_cond = any(any( isnan(A) | isinf(A) ) );   
complex_cond = any(any( imag(A) ~= 0 ) );
%
if complex_cond
    display(['Warning: Please separate the real and imaginary parts of ' ...
            sprintf('complex valued input functions.\n') ...
            'If the input functions are complex valued, ' ...
            sprintf('only the generalized Chebyshev quadratures\n') ...
            'will yield correct results.']);
end

if naninf_cond
    display(['Warning: Some values of the input functions on the'...
        ' interpolation grid are NaN/Inf.']) 
end

display('Constructing basis for input functions')
% Note to future self: Should do this svd randomized to speed it up.
[U,S] = svd(A,0);

num_sv = min( size(A) );

% Store singular values in sv, get numerical rank to precision e_comp
singular_values = diag(S);

rank = num_functions;
for ind = 1:num_sv
        
    if singular_values(ind) < algo_par.comp_eps
        rank = ind - 1;
        break
    end
    
end

% basis_values stores the actual values of the basis functions compressed
% to the numerical rank found in 4.
% We have to divide by the square-root-weights (Hilbert Space Isomorphism)

cheb_bval = bsxfun(@rdivide, U(:, 1:rank), sqrt_pw_w);

% Do Chebyshev interpolation on columns of basis_values to get an 
% orthonormal basis for the input functions.

basis_cell = cell( length( f_cheb_domain )-1, 1 );
 
ind = 0;
iter = 1;
while ind < total_length
    basis_cell{iter} = chebfun( ...
            cheb_bval( ind+1:f_exp_order(iter)+ind, :), ...
            [f_cheb_domain(iter), f_cheb_domain(iter+1)],...
            f_exp_order(iter) );
    ind = ind + f_exp_order(iter);
    iter = iter + 1;

end

% chebfun of basis
b_cheb = @(x) evaluate_piecewise_chebfun(x, basis_cell, f_cheb_domain);

display('Calculating integrals of basis functions.')
% calculate basis integrals w.r.t. weightfunctions
f_exps = zeros( size( f_cheb_domain ) );

if length( domain ) == length( exps )
    
    for i = 1 : length(domain)
        f_exps( domain(i) == f_cheb_domain ) = exps(i);
    end
    
end
    
if any( exps ~= 0 )
    % If there are singularities, Chebfun doesn't support vectorized
    % construction
    
    basis_integrals = zeros(1, rank);
    for i = 1:rank
        
        b_cheb_ind = @(x) evaluate_piecewise_chebfun_index(x, i,...
                            basis_cell, f_cheb_domain);
                        
        basis_integrals(i) = sum( chebfun( @(x) bsxfun( @times, ...
                            b_cheb_ind(x), ...
                            w_in(x) .* pw_weightfun(x, domain, exps) ...
                                                ) ...
                        , f_cheb_domain, 'exps', f_exps, 'splitting', 'on' ) );
    end
    
else
    % If input does not have singularities, do it vectorized
    basis_integrals = sum( chebfun( @(x) bsxfun( @times, b_cheb(x), ...
                            w_in(x) ) ...
                        , f_cheb_domain, 'splitting', 'on' ) );
    % if w_in = 1, could just do this:
    %basis_integrals = pw_w * b_cheb( pw_x );
end

% 6. construct generalized Chebyshev quadrature
display( 'Constructing generalized Chebyshev quadrature' );
[ch_x, ch_w] = calc_cheb_quad(U, rank, basis_integrals, pw_x, pw_w);

% 7. calculate generalized Gaussian quadrature
display( 'Constructing generalized Gaussian quadrature');

basis_prime_cell = cell( 1,length( f_cheb_domain )-1 );
%  
for iter = 1 : length(basis_cell)

    basis_prime_cell{iter} = diff( basis_cell{iter} );
    
end

% chebfun of basis derivative
%b_cheb_prime = chebfun( basis_prime_cell, f_cheb_domain, 'splitting', 'on' );
b_cheb_prime = @(x) evaluate_piecewise_chebfun(x, basis_prime_cell, f_cheb_domain);

basis_struct = struct('basis_integrals', basis_integrals,...
                      'rank', rank, ...
                      'b_cheb', b_cheb,...
                      'b_cheb_prime', b_cheb_prime);
               
[gg_x, gg_w, ~] = calc_gen_gauss_quad(ch_x, ch_w, basis_struct, algo_par);

% finally, have to scale weights to incorporate the pw weight function
ch_w = ch_w ./ transpose( pw_weightfun(ch_x, domain, exps) );
gg_w = gg_w ./ transpose( pw_weightfun(gg_x, domain, exps) );

end