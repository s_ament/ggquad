function [ sig ] = get_node_sig_eff( J, n, basis_struct)
% get_node_significance computes the significance of the nodes of a 
% subotimal quadrature rule (page 15 of Bremer, Gimbutas, Rokhlin)
% n is the current number of nodes and jac is the Jacobian of the nonlinear
% system

sig = zeros( n, 1 );
%sig2 = zeros(size(sig));

Jt = transpose(J);
JJt = J*Jt;
JJt_inv = inv(JJt);

I = eye(size(JJt));

% consider implementing the rank 1 update formula
for j = 1 : n
    %{
    % old computation
    Jk = J;
    Jk(:, j) = 0;
    Jk(:, j + n) = 0;
    
    sol = Jk \ basis_struct.basis_integrals;
    sig2(j) = norm( sol );
    %}
    % new computation
    Jkt = Jt;
    Jkt(j,:) = 0;
    Jkt(j + n,:) = 0;
    
    u = J(:,j);
    v = J(:,j+n);
    
    %JkJkt = JJt - u*u' - v*v';
  
    c_u = (1 - u' * JJt_inv * u);
    
    JkJkt_inv = (I + 1/c_u * JJt_inv * (u*u')) * JJt_inv;
    c_v = (1 - v' * JkJkt_inv * v);

    JkJkt_inv = (I + 1/c_v * JkJkt_inv * (v*v')) * JkJkt_inv;
    
    sol = Jkt * JkJkt_inv * transpose(basis_struct.basis_integrals);
    sig(j) = norm(sol);
    
    %{
    norm(sig2(j))
    norm(sig(j))
    norm(Jk * (sol-sol2))
    norm(Jk*sol-basis_struct.basis_integrals)
    norm(Jk*sol2-basis_struct.basis_integrals)
    %}
end

%{
[sig, ind] = sort(sig);
[sig2, ind2] = sort(sig2);
ind
ind2
pause
%}

end

