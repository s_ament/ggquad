function [ fx ] = evaluate_piecewise_chebfun_index( x, index, chebfun_cell, domain )

fx = NaN( size(x) );

len = length(domain);

for i = 1:len-2
    
   b_fun = chebfun_cell{i};
   
   x_cond = x >= domain(i) & x < domain(i+1);
   
   if any(x_cond)
        fx( x_cond ) = b_fun( x(x_cond), index );
   end
   
end

% Have to do last interval separately to handle last endpoint
b_fun = chebfun_cell{len-1};
   
x_cond = x >= domain(len-1) & x < domain(len);

if any(x_cond)
    fx( x_cond ) = b_fun( x(x_cond), index );
end

end

