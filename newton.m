function [ x, w, err ] = newton( x, w, n, basis_struct, algo_par)
% Multi-dimensional Newton method

warning('off', 'MATLAB:nearlySingularMatrix');
warning('off', 'MATLAB:rankDeficientMatrix');

int = algo_par.int;
max_it = algo_par.newton_max_it;
it = 0;

back_max_it = algo_par.back_max_it;
back_it = 0;

t_old = ones(2*n, 1) .* Inf;
t_new = [x; transpose(w)];

err = Inf;
    
while ( norm( t_new - t_old ) > eps ) && it <= max_it

    t_old = t_new;

    [J, sys_val] = get_jacobian(x, w, n, basis_struct);
    
    % direction of Newton step
    %{
    % Normal equations do not yield quadrature with minimum number of nodes
    % but yield to a quicker computation time
    JJt_inv = inv(J*J');
    dir = J'*JJt_inv*sys_val;
    %}
    
    dir = J \ sys_val;
    
    % first, make sure the Newton step does not send t_new out of the
    % interval
    % scale factor of Newton step
    step_size = algo_par.step_init;
    t_new = t_old - step_size * dir;
    step_size_temp = step_size;

    for i = 1:n
       
        if t_new(i) <= int(1)
            
            step_size_temp = ( t_old(i) - int(1) ) / ( 2 * dir(i) );

        elseif t_new(i) >= int(2)
            
            step_size_temp = ( t_old(i) - int(2) ) / ( 2 * dir(i) );
            
        end
        
        if step_size_temp < step_size
            step_size = step_size_temp;
        end

    end
    
    t_new = t_old - step_size * dir;
    x = t_new(1:n);
    w = transpose( t_new(n+1:2*n) );
    
    % Backtracking
    x_temp = t_new(1:n);
    w_temp = transpose( t_new(n+1:2*n) );
    sys_norm = norm(get_sys_val(x_temp, w_temp, basis_struct));
    
    while sys_norm > err && back_max_it > back_it
        step_size = step_size / algo_par.back_factor;
        t_new = t_old - step_size * dir;
        x_temp = t_new(1:n);
        w_temp = transpose( t_new(n+1:2*n) );
        sys_norm = norm(get_sys_val(x_temp, w_temp, basis_struct));
        back_it = back_it + 1;
    end
    
    if sys_norm < err
        x = t_new(1:n);
        w = transpose( t_new(n+1:2*n) );
        back_it = 0;
    else
        break
    end   
    
    it = it + 1;
    err = norm( get_sys_val(x,w, basis_struct) );
end

end

