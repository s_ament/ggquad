function [ fx ] = evaluate_piecewise_chebfun( x, chebfun_cell, domain )

fx = NaN( length(x), size( chebfun_cell{1}, 2) );

len = length(domain);

for i = 1:len-2
    
   b_fun = chebfun_cell{i};
   
   x_cond = x >= domain(i) & x < domain(i+1);

   if any(x_cond)
        fx( x_cond, : ) = b_fun( x(x_cond) );
   end
   
end

% Have to do last interval separately to handle last endpoint
b_fun = chebfun_cell{len-1};
   
x_cond = x >= domain(len-1) & x < domain(len);

if any(x_cond)
    fx( x_cond, : ) = b_fun( x(x_cond) );
end

end

