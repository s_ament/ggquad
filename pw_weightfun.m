function [ pw_wx ] = pw_weightfun( x, domain, exps )
% pw_weightfun computes a piecewise jacobi weightfunction, where the
% intervals are defined by the domain array, and the exponents by the exps
% array

left = @(x, e) (1+x).^( e );
right = @(x, e) (1-x).^( e );

% map from [a,b] to [-1,1]
intmap = @(x,a,b) (2*x - (a+b))/(b-a);

pw_wx = NaN(size(x));
numint = length( domain )-1;

if length( domain ) == length( exps )
    
    for i = 1 : numint

        intbol = (domain(i) <= x) & (x <= domain(i+1));
        xint = x( intbol );
 
        pw_wx( intbol ) = ...
            left(intmap(xint, domain(i), domain(i+1)), exps(i)) .* ...
            right(intmap(xint, domain(i), domain(i+1)), exps(i+1)) ...
            * ( (domain(i+1)-domain(i)) / 2 )^(exps(i)+exps(i+1));

    end
    
elseif 2*numint  == length( exps )
    
    for i = 1 : numint

        intbol = (domain(i) <= x) & (x <= domain(i+1));
        xint = x( intbol );

        pw_wx( intbol ) = ...
            left(intmap(xint, domain(i), domain(i+1)), exps(2*i-1)) .* ...
            right(intmap(xint, domain(i), domain(i+1)), exps(2*i)) ...
            * ( (domain(i+1)-domain(i)) / 2 )^(exps(i)+exps(i+1));

    end
    
else
    display('lengths of domain and exps arrays are not consistent');
end

end

