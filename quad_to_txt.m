function [ ] = quad_to_txt( x, w, str )

n = length(x);

% sort nodes
[x, ind] = sort(x);
w = w(ind);

fileID = fopen(str, 'w');

%{
% C
node_str = 'gx[%d] = %1.16e; \n';
weight_str = 'gw[%d] = %1.16e; \n';
%}

%
% Matlab
node_str = 'gx(%d) = %1.16e; \n';
weight_str = 'gw(%d) = %1.16e; \n';
%}

fprintf(fileID, 'num_quad = %d; \n', n);
for i = 1:n
    fprintf(fileID, node_str, i, x(i));
end

for i = 1:n
    fprintf(fileID, weight_str, i, w(i));
end

end

