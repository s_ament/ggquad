function [ gg_x, gg_w, n ] = calc_gen_gauss_quad( x, w, ...
                             basis_struct, algo_par)

N = basis_struct.rank;
n = basis_struct.rank;

while n > ceil(N/2)
   
    [J,~]  = get_jacobian(x, w, n, basis_struct);
    
    %
    % reorder the nodes according to significance
    sig = get_node_sig_eff(J, n, basis_struct);   
    [~, ind] = sort(sig);
    x = x(ind);
    w = w(ind);
    %}
    
    %{ 
    % or do it randomly
    p = randperm(n);
    x = x(p);
    w = w(p);
    %}
    k = 1;
    err = Inf;

    while (err >= algo_par.opt_eps || (isnan(err))) && k <= n

        display(sprintf('%d nodes left, trying to remove node %d', n, k));
        x_temp = x(1:n ~= k);
        w_temp = w(1:n ~= k); 
        
        [x_temp, w_temp, err] = newton( x_temp, w_temp, n-1,...
                                        basis_struct, algo_par);
        display(sprintf('current optimization error: %e', err))
        k = k + 1;
        
    end
    
    if err < algo_par.opt_eps
        
        x = x_temp;
        w = w_temp;
        n = n - 1;
        
    else
        break;
    end
    
end

gg_x = x;
gg_w = w;

end

